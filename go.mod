module homework

go 1.19

require (
	github.com/JohannesKaufmann/html-to-markdown v1.4.1
	github.com/go-chi/chi v1.5.5
	github.com/gomarkdown/markdown v0.0.0-20230922112808-5421fefb8386
)

require (
	github.com/PuerkitoBio/goquery v1.8.1 // indirect
	github.com/andybalholm/cascadia v1.3.2 // indirect
	golang.org/x/net v0.17.0 // indirect
)
