package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAPIHelloHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(apiHelloHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := "Hello from API"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestReverseProxy(t *testing.T) {
	reqAPI, err := http.NewRequest("GET", "/api/", nil)
	if err != nil {
		t.Fatal(err)
	}

	reqOther, err := http.NewRequest("GET", "/some-path", nil)
	if err != nil {
		t.Fatal(err)
	}

	rrAPI := httptest.NewRecorder()
	rrOther := httptest.NewRecorder()

	// Создаем фейковый сервер для обработки запросов к серверу Hugo
	hugoServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello from Hugo"))
	}))
	defer hugoServer.Close()

	// Создаем экземпляр ReverseProxy с URL сервера Hugo
	proxy := NewReverseProxy("", "")

	// Создаем обработчик с использованием ReverseProxy
	handler := proxy.ReverseProxy(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/api/" {
			w.Write([]byte("Hello from API"))
		} else {
			http.Error(w, "Not Found", http.StatusNotFound)
		}
	}))

	handler.ServeHTTP(rrAPI, reqAPI)
	handler.ServeHTTP(rrOther, reqOther)

	if status := rrAPI.Code; status != http.StatusOK {
		t.Errorf("обработчик вернул неверный код статуса для запроса с префиксом /api/: получено %v, ожидается %v", status, http.StatusOK)
	}

	expectedAPI := "Hello from API"
	if rrAPI.Body.String() != expectedAPI {
		t.Errorf("обработчик вернул неправильное тело ответа для запроса с префиксом /api/: получено %v, ожидается %v", rrAPI.Body.String(), expectedAPI)
	}

	if status := rrOther.Code; status != 502 {
		t.Errorf("обработчик вернул неверный код статуса для запроса без префикса /api/: получено %v, ожидается %v", status, http.StatusNotFound)
	}
}
