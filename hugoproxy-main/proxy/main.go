package main

import (
    "fmt"
    "github.com/go-chi/chi"
    "log"
    "net/http"
    "net/http/httputil"
    "net/url"
    "os"
    "time"
)

func main() {
    r := chi.NewRouter()
    proxy := NewReverseProxy("localhost", "8080")
    r.Use(proxy.ReverseProxy)
    r.Get("/api/", apiHelloHandler)
    http.ListenAndServe(":8080", r)
}

func apiHelloHandler(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("Hello from API"))
}

type ReverseProxy struct {
    host string
    port string
}

func NewReverseProxy(host, port string) *ReverseProxy {
    return &ReverseProxy{
        host: host,
        port: port,
    }
}

func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        hugoURL, _ := url.Parse("http://hugo:1313")
        hugoProxy := httputil.NewSingleHostReverseProxy(hugoURL)
        if r.URL.Path == "/api/" {
            w.Write([]byte("Hello from API"))
            return
        }
        hugoProxy.ServeHTTP(w, r)
    })
}

const content = ``

func WorkerTest() {
    t := time.NewTicker(1 * time.Second)
    var b byte = 0
    for {
        select {
        case <-t.C:
            err := os.WriteFile("/app/static/_index.md", []byte(fmt.Sprintf(content, b)), 0644)
            if err != nil {
                log.Println(err)
            }
            b++
        }
    }
}
